
// F3 - Driver Identification Data
let F3 = (F, pField) => {
    // No-op for the present.
};
// F4 - Device Identification
let F4 = (F, pField) => {
    // No-op for the present.
};
// F5 - Measured Values
let F5 = (F, pField) => {
    // No-op for the present.
};
// F7 - Analogue Data 
let F7 = (F, pField) => {
    // No-op for the present.
};

// F8 - Mime Data 
let F8 = (F, pField) => {
    // No-op for the present.
};

// F27 - Odometer Readings 
let F27 = (F, pField) => {
    // No-op for the present.
};

// F28 - Cell Tower Data
let F28 = (F, pField) => {
    // No-op for the present.
};