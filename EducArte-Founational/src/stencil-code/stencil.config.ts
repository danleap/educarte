import { Config } from "@stencil/core";
import { readFileSync } from "fs";

// https://stenciljs.com/docs/config

export const config: Config = {
  devServer: {
    https: {
      cert: readFileSync("cert.pem", { encoding: "utf-8" }),
      key: readFileSync("key.pem", { encoding: "utf-8" })
    }
  },
  outputTargets: [
    {
      type: "www",
      serviceWorker: null
    }
  ],
  globalScript: "src/global/app.ts",
  globalStyle: "src/global/app.css"
};
