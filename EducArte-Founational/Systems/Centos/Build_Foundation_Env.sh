NGINX_REPO_FILE=./nginx_repo_file.repo
GRAFANA_REPO_FILE=./grafana_repo_file.repo
INLUX_REPO_FILE=./influx_repo_file.repo

function installGrafana(){
    if [ -d '/etc/yum.repos.d/' ]; then 
        if [ ! -f '/etc/yum.repos.d/grafana.repo' ]; then
            cat "./$GRAFANA_REPO" > /etc/yum.repos.d/grafana.repo
            sudo yum install epel-release
            sudo yum install nginx
        fi
    fi
}

function installInfuxDB(){
    if [ -d '/etc/yum.repos.d/' ]; then 
        if [ ! -f '/etc/yum.repos.d/influx.repo' ]; then
            cat "./$INFLUX_REPO" > /etc/yum.repos.d/influx.repo
            sudo yum install epel-release
            sudo yum install nginx
        fi
    fi
}

function installNGINX(){
    if [ -d '/etc/yum.repos.d/' ]; then 
        if [ ! -f '/etc/yum.repos.d/nginx.repo' ]; then
            cat "./$NGINX_REPO" > /etc/yum.repos.d/nginx.repo
            sudo yum install epel-release
            sudo yum install nginx
        fi
    fi
}

function execute(){
    installNGINX
    sudo systemctl enable nginx
    sudo systemctl start nginx
    installInfuxDB
    installGrafana
}

execute