#!/usr/bin/env node

const  express                   = require('express');
const  bodyParser                = require('body-parser');
const  winston                   = require('./winston/winston')(__filename);
const  stinger                   = require('./clients/stinger');
const  path                      = require('path');
const  csv                       = require('fast-csv');
const { onUpload, onDeleteFile } = require('./lib/fileuploader');

//  test app for Sting sensor input
const app = express();

//  for uploading csv files
const public = path.join(__dirname, './public');
app.use('/upload', express.static(public))

//  create application/json parser
const jsonParser = bodyParser.json()

//  create application/x-www-form-urlencoded parser
const urlencodedParser = bodyParser.urlencoded({ extended: false });

//  Port for uploading data
const PORT = 8081;

const doLogin = (req, res) => {
    if (!req.body) return res.sendStatus(400)
    res.send('welcome, ' + req.body.username)
};

const doJson = (req, res) => {
    if (!req.body) return res.sendStatus(400)
    // create user in req.body
};

const read154197 = (req, res) => {
    const success = (d) => {
        res.send(JSON.stringify(d));
    }
    stinger.start();
    stinger.influxRead('154197').then(success, () => res.send('Error influxRead'));
};

const testCSV = (req, res) => {
    const success = () => {
        res.send('Test CSV File(s) Write Success');
    }
    let csvData = req.body;
    console.log("  testCSV " + JSON.stringify(req.body))
    stinger.start();
    stinger.writeCSV( csvData ).then(success, () => res.send('Test CSV File(s) Write Fail'));
}

const writeJSON = (req, res) => {
    const success = (json) => {
        res.send('Digital Matter JSON File(s): Write Success to Bigchain and Influx');
    };
    const failure = () => {
        res.send('Digital Matter JSON File(s): Write Fail to Bigchaiin and Influx');
    };
    let json = req.body;
    json = json ? json : {status: 'Digital Matter JSON File(s) Write Failure'};
    stinger.start();
    stinger.writeDMSensorData(json).then(success, failure);
}

const DMconnectTest = (req, res) => {
    const success = (json) => {
        res.send('JSON File(s) Write Success to Influx');
    };
    const failure = () => {
        res.send('JSON File(s) Write Fail to Influx');
    };
    const ISTEST = false; //set as needed
    let json = req.body;
    json = json ? json : {status:'Test JSON File(s) Write Failure'};
    stinger.start();
    //uncomment or comment as apropropriate
    ISTEST ? res.json(json) : stinger.writeJSON(json).then(success, () => res.send('JSON File(s) Write Fail'));
    //stinger.writeJSON(json).then( success(json), failure);
}

const uploadJSON = (req, res) => {

    const success = (json) => {
        res.send('JSON File(s) Write Success to Bigchain\n');
    };

    const failure = () => {
        res.send('JSON File(s) Write Fail to Influx');
    };

    const json = req.body;
    const TEST = true;
    stinger.start();
    jsonPacket = json ? json : {status:'JSON File(s) Write Failure\n'};
    winston.info("Starting to write JSON data \n" + JSON.stringify(json,null,4) + "\n")
    if ( TEST ){
        stinger.uploadJSON(jsonPacket).then( success(jsonPacket), failure);
    } else {
        stinger.writeJSON(jsonPacket).then( success(jsonPacket), failure);
    }
}

const showVIZ = (rq, res) => {
    res.sendFile("./index.html", {
        root: __dirname
    });
};

app.post('/login', urlencodedParser, doLogin);
app.post('/api/users', jsonParser, doJson);
app.get('/api/visualise', showVIZ); 
app.get('/api/read154197', read154197);
app.post('/api/writeCSV', testCSV);
app.post('/api/writejson', jsonParser, DMconnectTest);
app.post('/api/testjson', jsonParser, uploadJSON);
app.post('/uploads', onUpload);

app.listen(PORT, '0.0.0.0', () => {
    console.log(`Stinger app listening on port ${PORT}!`)
    winston.info(`Stinger app listening on port ${PORT}!`)
});
