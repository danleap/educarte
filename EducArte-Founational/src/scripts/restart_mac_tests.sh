#!/bin/bash
#  For the moment, the script assumes that inlfux DB and mongo 
#  DB are running and that bigchaindb and tendermint have been 
#  terminated.s

/bin/rm -fr ~/.tendermint
tendermint init 
sudo mongo --dbpath=/var/lib/mongo &
bigchaindb start &
tendermint node --proxy_app=kvstore --consensus.create_empty_blocks=false &