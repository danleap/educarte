'use strict';

/**
 * Certain chars need to be escaped for certain string portions.
 * This function will escape the required items for a given input string
 *
 * @param  {Array}  replacers
 * @param  {String} input
 * @return {String}
 */
function escapeChars (replacers, input) {
  for (var r in replacers) {
    input = input.replace(replacers[r][0], replacers[r][1]);
  }
  return input;
};

/**
 * Creates a tag or value string that can be incldued in a complete InfluxDB
 * line protocol string
 * @param  {Array}  keys
 * @param  {Object} data
 * @param  {Function} generator - method to generate the tag/field string
 * @param  {Boolean} flag - run check on data and wrap/append if necessary

 * @return {String}
 */
function generateTagFieldString(data, keys, generator, flag) {
    const SPACE_REPLACER = [new RegExp(' ', 'gi'), '\\ '];
    const COMMA_REPLACER = [new RegExp(',', 'gi'), '\\,'];
    const EQUAL_REPLACER = [new RegExp('=', 'gi'), '\\='];
    const FIELD_TAG_REPLACERS = [SPACE_REPLACER, COMMA_REPLACER, EQUAL_REPLACER];
    let postprocessing = d => d; //identity function
    let ret = '';
    let key = null;
    let d = null;
    postprocessing = !flag ? postprocessing : (d) => { //run checks and wrap/append
        if(typeof d === 'string') {
            d = (d.toLowerCase() === 'true' || d.toLowerCase() === 'false') ? d : (d.toLowerCase() === 't' || d.toLowerCase() === 'f') ? d : '"' + d + '"';
        }
        d = (typeof d === 'number' && Number.isInteger(d)) ? d + 'i' : d;
        return d;
    };
    // Keys can be provided. If not we assume all keys are required
    keys = keys || Object.keys(data);
    if(keys.length < 1) return ret;
    for (var i in keys) {
        key = keys[i];
        d = postprocessing(data[key]); //take care of strings, integers and bools in field values
        ret += generator(key, d, FIELD_TAG_REPLACERS);
    }
    ret = ret.slice(0, -1);
    return ret;
};
exports.generateTagString = function (data, keys) {
    let generator = function(k, d, r) {
        return escapeChars(r, k) + '=' + escapeChars(r, d) + ',';
    }
    // Keys can be provided. If not we assume all keys are required
    return generateTagFieldString(data, keys, generator);
};
exports.generateFieldString = function (data, keys) {
    let generator = function(k, d, r) {
        return escapeChars(r, k) + '=' + d + ',';
    }
    return generateTagFieldString(data, keys, generator, true); //last param is to postprocess data
};

//fix bug of missing function
exports.escapeMeasureName = function(measurement) { 
    const SPACE_REPLACER = [new RegExp(' ', 'gi'), '\\ '];
    const COMMA_REPLACER = [new RegExp(',', 'gi'), '\\,'];
    const MEASURE_REPLACERS = [SPACE_REPLACER, COMMA_REPLACER];

    return escapeChars(MEASURE_REPLACERS, measurement); 
};

/**
 * Generates a line protocol string from provided inputs
 * @param  {String} m  measurement name
 * @param  {String} t  tags
 * @param  {String} f  fields
 * @param  {String} ts [timestamp]
 * @return {String}
 */
exports.generateLineProtocolString = function (params) {
  params.ts = params.ts ? ' ' + params.ts : '';
    // tags are also optional, but we need to pad accordingly if they're missing
  params.tags = (params.tags && params.tags.length !== 0) ? ',' + params.tags : '';
  return params.measurement + params.tags + ' ' + params.fields + params.ts;
};
