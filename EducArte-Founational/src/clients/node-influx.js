require('dotenv').config();               //  Get the Environment variables for this application.
const _        = require('lodash');       //
const moment   = require('moment');       //  We use 'moment' for timestamp'ing data.
const fetch    = require('node-fetch');   //  We use 'fetch' to post data
const Influx   = require('influx');       //  Influx database driver
const winston  = require('../winston/winston')(__filename);

const TIMEOUT = 10000; //How long to wait between pings

//----------  Influx Database Interface  ----------------------------------------------------------
//  The NodeInflux class provides methods for reading and writing streams of points to and
//  from an Inlfux database. The config file for the database is stored in the following
//  default directory:  influxd -config /usr/local/etc/influxdb.conf

class NodeInflux {

    constructor(config, Schema) {
        //  The initial configuration will be given by an external
        //  schema definition. The current schema definition is
        //  given externally in Schema.0.0.4.js.
        //  ------------------------------------------------------
        //  Each field is a JS 'getter'.
        config.schema = [{
            measurement: Schema.measurement,
            fields:      Schema.fields,
            tags:        Schema.tags,
        }];
        this._influx = new Influx.InfluxDB(config);
        this.config = config;
    } //  End of Constructor

    async ping() {

        winston.info(`Pinging Influx with timeout ${TIMEOUT}`);

        let online = false;
        const success = ( hosts ) => {
                hosts.map( host => {
                        host.online ? winston.info(`${host.url.host} responded in ${host.rtt}ms running ${host.version})`) : winston.info(`${host.url.host} is offline :(`);
                        online = online || host.online;
                });
                return online;
        };
        const failure = () => {
                return online = false;
        };
        try {
            return this._influx.ping(TIMEOUT).then(success,failure);
        } catch(err) {
            winston.error(err);
            return false;
        };
    }  // End of 'ping'

    async init() {
        return this._influx.getDatabaseNames()
                   .then( names => {
                            if (!names.includes(this.config.database)) {
                                winston.info(`Creating Influx database ${this.config.database}`);
                                return this._influx.createDatabase(this.config.database);
                            } else {
                                return Promise.resolve(this.config.database);
                             }
                    });
    }  //  End of 'init'

    async readPoints(measurement) {
        const query = () => {
            return this._influx.query( `select * from "${measurement}" order by time desc` )
        };
        const list = ( rows ) => {
            rows.forEach( row => console.log(`A request to ${measurement} temp: ${row.Temperature_C} at ${row.Logged}`))
            return rows;
        };
        const failure = (err) => {
            console.log(err)
        };
        return measurement ? query().then(list, failure) : Promise.reject(`No measurement provided to Influx.readPoints`);
    }  // End of 'readPoints'

    async writePoints(data) {
        try {
            winston.info(" node-influx writePoints : " + JSON.stringify(data, null, 4));
            //  First define options for measurement precision and then get the measurement
            //  name from the data schema passed in.
            const options     = { precision: 's' };
            const measurement = data.length > 0 ? data[0].measurement : null;

            const query = () => {
                return this._influx.query( `select * from "${measurement}" order by time desc` )
            };
            const list = ( rows ) => {
                //  winston.info("Listing rows")
                rows.forEach(row => console.log(` ${measurement} temp: ${row.Temperature_C} at ${row.Logged} / time ${row.time} hash: ${row.BigChain_Hash}`))
            };
            const failure = (err) => {
                winston.error("writePoints error " + err);
                Promise.reject(err);
            };

            if(!measurement) { return winston.error(`Influx.writePoints data length = 0`)};

            await this.init();
            winston.info( "Points to be written " + JSON.stringify(data, null, 4) );
            this._influx.writePoints(data, options)
                .then(query, failure)
                .then(list, failure)
                .catch( (err) => Promise.reject(err));
        } catch(err) {
            return Promise.reject(err);  //  Throwing an error triggers a retry.
        }
    }  //  End of 'writePoints'
}  //  End of NodeInflux

module.exports = NodeInflux;
