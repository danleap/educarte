const moment = require('moment');
const _      = require('lodash');

const Schema    = require('./schema.0.0.4'); //0.0.3 combines RB and DM, plus str -> fieldtype conversion
const rbFields  = require('./RB_Field_Structure.json');
const rbTags    = require('./RB_Tag_Structure.json');

class RoamBee {

    constructor(measurement) {
        this.measurement  = measurement;
        this._point       = null;
        this._pentaTags   = rbTags;
        this._pentaFields = rbFields;
    }

    preprocess(d, _d) {
       _.assign(d, _d); //use lodash implementation
        return d;
    }

    makePoint(d) {

        const renameKeys = (keysMap, obj) => Object.keys(obj).reduce((acc, key) => ({
            ... acc,
            ... keysMap[key] ? { [keysMap[key] || key]: obj[key] } : {}
        }), {});
        const convertFieldTypes = (o) => {
            Object.keys(o).map(key => {
                if(o[key] === undefined || o[key] === null) {
                    delete o[key];
                    return;
                }
                const fieldType = Schema.fields[key];
                o[key] = Schema.convertType(fieldType, o[key]);
            });
            return o;
        };
        const convertFields = (o) => {
            //loop over Schema converters and apply to the appropriate input field
            Schema.converters.map((c) => {
                const val = o[c.input]; //check that this input exists, null, falsy are OK
                if(val === undefined ) { return; }
                o[c.output] = (val * c.scaling) + c.offset;
            });
            return o;
        };
        const prepareTimestamp = (_d) => {
            //   { '﻿Date Logged (AET (+10:00))': '27/03/2019 20:52',
            let ts = null;
            let logged = null;
            let received = null;
            _.map(Object.keys(_d), (key) => {
                const flag1 = key.includes('(UTC +00:00)');
                const flag2 = key.includes('Received');
                if(!flag1 && !flag2) { return; }
                if(flag1) {
                    const months = {Jan:0 ,Feb:1, Mar:2, Apr:3, May:4, Jun:5, Jul:6, Aug:7, Sep:8, Oct:9, Nov:10, Dec:11};
                    let hour = null;
                    let minute = null;
                    let day = null;
                    let month = null;
                    let year = null;
                    let date = _d[key];
                    logged = date;
                    // Apr 26, 2019 10:39 PM
                    date = date.split(" ");
                    day = date[1].slice(0,date[1].length - 1);
                    month = months[date[0]];
                    year = date[2];
                    date[3] = date[3].split(':');
                    hour = date[3][0];
                    hour = parseInt(hour, 10);
                    hour = date[4] === 'PM' ? hour + 12 : hour; //convert to integer
                    hour = hour + ''; //back to a string
                    minute = date[3][1];
                    ts = moment({ year:year, month:month, day:day, hour:hour, minute:minute}).toDate();
                }
                if(flag2) {
                    received = _d[key];
                }
            })
            return {ts:ts, logged:logged, received: received};
        }; //End of prepareTime

        const tags = renameKeys( this._pentaTags, d);
        let fields = renameKeys(this._pentaFields, d);
        fields = convertFieldTypes(fields);
        fields = convertFields(fields);
        const point = {
            measurement: this.measurement,
            tags: tags,
            fields: fields
       };
       const timestamp = prepareTimestamp(d);
       if(_.isDate(timestamp.ts)) {
            point.timestamp = timestamp.ts;
            point.fields.Logged = timestamp.logged;
            point.fields.Time_Received = timestamp.received;
       }
       return point;
    }

    pointFromCSV(d) {
        return this.makePoint(d);
    }
}

module.exports = RoamBee;
