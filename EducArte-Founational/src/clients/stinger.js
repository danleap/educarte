require('dotenv').config(); //GET THE ENVIRONMENT
const exitHook               = require('exit-hook'); //clean up on exit
const _                      = require('lodash'); //goodies
const fromCSV                = require('rx-from-csv').fromCSV; //stream the csv file
const { map, bufferCount }   = require('rxjs/operators'); //do posts sequentially
const { from }               = require('rxjs');
const winston                = require('../winston/winston')(__filename);
//const CorlysisInflux = require('./clients/corlysis'); //switch from Corlysis platform
const NodeInflux             = require('./node-influx');
const Schema                 = require('../data/schema.0.0.4'); //0.0.1 is Corlysis
//const DigitalMatter          = require('../data/digitalmatter'); //moved to Sensors obj. to enable additional Sensor platforms
const isReachable            = require('is-reachable');
const blockchain             = require('./blockchain.js');
const moment                 = require('moment');
const jsonfile = require('jsonfile')

let config = { //one time init from .env file
    protocol: process.env.INFLUX_PROTOCOL,
    host: process.env.INFLUX_HOST,
    port: process.env.INFLUX_PORT,
    username: process.env.INFLUX_USERNAME,
    password: process.env.INFLUX_PASSWORD,
    database: process.env.INFLUX_DATABASE
};
config = _.pickBy(config, val => !_.isNil(val)); //use only defined values, skip otherwise
const serverURL = () => `${config.protocol}://${config.host}:${config.port}`;
const uuidv4 = () => { //mock guid generator
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
};
const TESTURL = 'https://apple.com'; //for checking we are online
const NUMPOINTS = 25; //IPoint buffer size
const MAXRETRY  = 5; //how many times we will retry online check before failing, 10s timeout between each
const PREFIXDATA = { //concatenate fixed data (for now) with dynamic sensor data
    Company: 'Juan Valdez Columbia',
    Project: 'Columbia Pilot Project',
    Status: 'data point(s) tests',
    Tag_Data_Hash: uuidv4(), //mock data
    BigChain_Hash: uuidv4(),
    Iota_Hash: uuidv4(),
    Penta_Hash: uuidv4()
};
const PREFIXDATAOBFUSCATED = { //concatenate fixed data (for now) with dynamic sensor data
    Company: 'Juan Valdez Columbia',
    Project: 'Columbia Pilot Project',
    Status: 'data point(s) tests',
    Tag_Data_Hash: uuidv4(), //mock data
    BigChain_Hash: uuidv4(),
    Iota_Hash: uuidv4(),
    Penta_Hash: uuidv4()
};
const Sensors = {
    DigitalMatter: require('../data/digitalmatter'),
    RoamBee: require('../data/RoamBee')
}
// We declare the blockchain at the outer-most level to ensure that
// the 'Salt' persists.
const blockchainDriver = new blockchain.Blockchain_Driver();

const influxRead = async (measurement) => {
    influx = new NodeInflux(config, Schema); //which sink to write to, resolve promise with value for next iteration
    const url = serverURL();
    let retries = MAXRETRY;
    const reachable = await isReachable(TESTURL, {timeout:10000}).catch((err) => winston.error(err));
    if(!reachable) { return winston.info(`Server at URL ${url} not reachable`)}
    while (retries-- > 0 && !await influx.ping() ) {} //old school algorithm */
    return influx.readPoints(measurement);
}

//d = { file, measurement}
const influxWrite  = async (d, sensorType) => { //given a csv file, loop over data and write to sink, group by measurements
    let sensor = null;
    let PREFIX = _.defaults({}, PREFIXDATA);
    winston.info(`influxWrite: ${JSON.stringify(d)} \n`);
    Schema.measurement = d.measurement;
    //for Apple data obfuscate the metadata, comment out otherwise
    PREFIX = _.defaults({}, PREFIXDATAOBFUSCATED);
    switch(sensorType) {
        case 'RoamBee':
            sensor = new Sensors.RoamBee(Schema.measurement);
        break;
        default:
            sensor = new Sensors.DigitalMatter(Schema.measurement); //pick which platform to work with
    }
    //digitalMatter = new Sensors.DigitalMatter(Schema.measurement); //transform DM data to Influx IPoint
    influx = new NodeInflux(config, Schema); //which sink to write to, resolve promise with value for next iteration

    /* let retries = 3; //test that server really is online, total of 30s timeout before giving up, each ping 10s timeout
    while (retries-- > 0 && !(online = await influx.ping()) ) {} //old school algorithm */

    return new Promise((resolve, reject) => { //OK now start writing data

        const onNext = async (data) => { //write array of data to influx
            const url       = serverURL();
            let filepath    = d.file;
            filepath        = filepath.split('.csv');
            filepath        = filepath[0] + '.json';
            jsonfile.writeFileSync(filepath, data);
            let retries     = MAXRETRY;
            const reachable = await isReachable(TESTURL, {timeout:10000}).catch((err) => winston.error(err));
            if(!reachable) { return winston.info(`Server at URL ${url} not reachable`)}
            while (retries-- > 0 && !await influx.ping() ) {} //old school algorithm.

            // First write all the data points to the blockchain.
            let txData = []
            await blockchainDriver.writeTransactions( data )
                    .then( (txRetrievedData) => {
                        txData = txRetrievedData;
                    })
                    .catch((err) => winston.error("Blockchain Driver Error : " + err));
            // Now write all the data points to Influx with the transaction hash added.
            // let txData = _.isArray(data) ? data : [data];
            txData = data
            influx.writePoints( txData );
        }
        const onError = (err) => {
            console.log(err)
            winston.error(err);
         };
        const onComplete = () => {
            winston.info('Influx Data Write Completed');
            resolve(); //done with this file, resolve promise and go to the next file
        };

        fromCSV(d.file).pipe(
            map((data) => { //append the measurement id to the line data
                PREFIX.SensorID = data.SensorID || Schema.measurement;
                data = sensor.preprocess(data, PREFIX); //map data onto standard
                data = sensor.pointFromCSV(data);
                return data; //emit data along
            }),
            bufferCount(NUMPOINTS) //put individual csv rows into an array of 25
        ).subscribe(onNext, onError, onComplete); //writeData returns a promise which when resolves calls onNext
    }); //eo Promise

}; //End of doInflux

const writeDMSensorData = async ( dataPoint, sensorType ) => { //look at line #125, dataPoint === d?
    let sensor = null;
    Schema.measurement = dataPoint.SerNo;
    switch(sensorType) {
        default: sensor = new Sensors.DigitalMatter(Schema.measurement); //pick which platform to work with
    }
    //digitalMatter      = new Sensors.DigitalMatter( Schema.mesurement );
    influx             = new NodeInflux(config, Schema);
    return new Promise( (resolve, reject) => {
        const onNext = async (data) => {
            const url       = serverURL();
            let retries     = MAXRETRY;
            const reachable = await isReachable(TESTURL, {timeout:10000}).catch((err) => winston.error(err));
            if(!reachable) { return winston.info(`Server at URL ${url} not reachable`)}
            while (retries-- > 0 && !await influx.ping() ) {} //old school algorithm */

            data = sensor.prepareData( dataPoint );
            let tx = await blockchainDriver.writeTransactions( data ).catch((err) => winston.error(err));
            data.BigChain_Hash = tx ? tx.id : "";
            console.log("Bigchain Hash Key " + data.BigChain_Hash);
            influx.writePoints( data );
        }; //eo onNext
        const onError = (err) => {
            console.log(err)
            winston.error(err);
         }; //eo onError
        const onComplete = () => {
            winston.info('JSON Data Write Completed');
            resolve(); //done with this file, resolve promise and go to the next file
        };  //eo onComplete
        from(dataPoint).pipe(
            bufferCount(NUMPOINTS) //put individual csv rows into an array of 25
        ).subscribe(onNext, onError, onComplete);
    });
}; //eo writeDMSensorData

const writeJSON  = async (d, sensorType) => { //given a csv file, loop over data and write to sink, group by measurements
    let sensor = null;
    let PREFIX = PREFIXDATA;
    d = _.isArray(d) ? d : [d];
    winston.info(`writeJSON : ${JSON.stringify(d)}`);

    //////////////////////////////////////////
    // We have assumed that each sensor reading in the spreadsheet contains
    // a Sensor_ID that can be used as the measurement.
        Schema.measurement = d[0].SensorID;
    // Schema.measurement is used everywhere
    //////////////////////////////////////////
    //pass in the sensor type, default DigitalMatter
    switch(sensorType) {
        case 'RoamBee':
            sensor = new Sensors.RoamBee(Schema.measurement);
            PREFIX = PREFIXDATAOBFUSCATED;
        break;
        default: sensor = new Sensors.DigitalMatter(Schema.measurement); //pick which platform to work with
    }
   // digitalMatter = new Sensors.DigitalMatter(Schema.measurement); //transform DM data to Influx IPoint
    influx = new NodeInflux(config, Schema); //which sink to write to, resolve promise with value for next iteration
    /* let retries = 3; //test that server really is online, total of 30s timeout before giving up, each ping 10s timeout
    while (retries-- > 0 && !(online = await influx.ping()) ) {} //old school algorithm */
    console.log("writeJSON return promise \n")
    return new Promise((resolve, reject) => { //OK now start writing data
        const onNext = async (data) => { //write array of data to influx
            const url      = serverURL();
            let retries     = MAXRETRY;
            const reachable = await isReachable(TESTURL, {timeout:10000}).catch((err) => winston.error(err));
            if(!reachable) { return winston.info(`Server at URL ${url} not reachable`)}
            while (retries-- > 0 && !await influx.ping() ) {} //old school algorithm */
            // First write the data to the blockchain.
            let tx = await blockchainDriver.writeTransactions( data ).catch((err) => winston.error(err));
            data.BigChain_Hash = tx ? tx.id : "";
            // Second write to influx.
            influx.writePoints( data );
            winston.info("Influx DB: Blockchain and Transaction written .. \n")
        }
        const onError = (err) => {
            console.log(err)
            winston.error(err);
         };
        const onComplete = () => {
            winston.info('JSON Data Write Completed');
            resolve(); //done with this file, resolve promise and go to the next file
        };
        from(d).pipe( //read the input array line by line
            // take(3), //for testing, comment out for production
            map((data) => { //append the measurement id to the line data
                //  data.measurement = d.measurement; //add measurement => '154197' to data being streamed
                PREFIX.SensorID = Schema.measurement;
                data = sensor.preprocess(data, PREFIX);
                data = sensor.pointFromCSV(data);
                return data; //emit data along
            }),
            bufferCount(NUMPOINTS), //put individual csv rows into an array of 25
        ).subscribe(onNext, onError, onComplete); //writeData returns a promise which when resolves calls onNext
    }); //eo Promise

}; //eo writeJSON


const writeCSV = async (_d, sensorType) => { //can pass in an array or dataFileObject: {file, measurement}

    /* _d = _d || [ //downloaded csv data and which influxdb measure to assign to
        {file:'./data/DeviceTelemetryData_154197_27-03-2019.csv', measurement:'154197'},
        {file:'./data/DeviceTelemetryData_154197_29-03-2019.csv', measurement:'154197'},
        {file:'./data/DeviceTelemetryData_154416_29-03-2019.csv', measurement:'154416'}
    ]; */
    _d = _d || [ //downloaded csv data and which influxdb measure to assign to
        {file:'./Scaffold/Digital_Matter/data/archive/DeviceData_154416_10-04-2019.csv', measurement:'154416'}
    ];
    _d = Array.isArray(_d) ? _d : [_d]; //handle case of passing in a single object
    for(let d of _d) { //loop over the files and await until one is done before going on to the next
        try { //process sequentially, if offline then catch error
            await influxWrite(d, sensorType); //process files sequentially, wait until one is done before moving to next
        } catch(err) { //oops, something went wrong
            winston.error(err);
        }
    }
    return Promise.resolve();
}; //eo writeCSV

const start = () => { //call to set the exitHook and write to the log that all is good
    winston.info(`Starting Sting.${process.env.VERSION}`);
    //what to do when we finish
    exitHook(() => {
        winston.info(`Exiting Sting.${process.env.VERSION}`);
    });
};

//start everything by calling a test function
//todo: wrap everything in an Express app to handle routes

//writeCSV();
//influxRead('154197');

module.exports = {
    start: start,
    writeJSON: writeJSON,
    writeCSV: writeCSV,
    influxWrite: influxWrite,
    influxRead: influxRead
}
