require('dotenv').config(); //GET THE ENVIRONMENT
const _ = require('lodash'); //goodies
const moment = require('moment'); //for timestamp handling
const jsonToInfluxdbLine = require('../lib/json-to-influxdb-line/json-to-influxdb-line');
const fetch = require('node-fetch'); //post data
const winston  = require('../winston/winston')(__filename);

let counter = 0; //keep track of data points within module

class CorlysisInflux {
    constructor() {}

    writeData(d) {
       /*  const _d = [ //downloaded csv data and which influxdb measure to assign to
            {file:'./data/DeviceTelemetryData_154197_27-03-2019.csv', measurement:'154197'},
            {file:'./data/DeviceTelemetryData_154197_29-03-2019.csv', measurement:'154197'},
            {file:'./data/DeviceTelemetryData_154416_29-03-2019.csv', measurement:'154416'}
        ]; */
        const index = 0; //for testing purposes use only one of the d files, loop over in production
        const startDate = process.env.CORLYSIS_STARTDATE + ' 00:00';
        const start = moment(startDate, 'DD/MM/YYYY HH:mm').valueOf();
        const minus7 = moment().hour(0).minute(0).second(0).millisecond(0).subtract(7, 'days').valueOf();
        const delta = minus7 - start; //how much to offset time data by, move forward by 7 days
        const TIMEOFFSET = true; //do we offset all the timesteps by 7 days in the past from today
        const measurement = d.measurement; //from above since the csv data does not have the sensor id embedded
        const body = () => { //generate influxdb line protocol from data
            const _tags = {
                status: 'Log Reason',
                sensorid: 'Project Code',
                project_code: 'Project Code'
            }; //eo _tags
            const _fields = {
                Latitude: 'Latitude',
                Longitude: 'Longitude',
                Speed_KmH: 'Speed KmH',
                Speed_Accuracy_KmH: 'Speed Accuracy (KmH)',
                Heading_Degrees: 'Heading Degrees',
                Altitude_m: 'Altitude (m)',
                Position_Accuracy_m: 'Position Accuracy (m)',
                PDOP_Raw: 'PDOP Raw',
                Gps_Status_Fix_OK: 'Gps Status Fix OK',
                Gps_Status_Fix_3D: 'Gps Status Fix 3D',
                Driver_Id_Type: 'Driver Id Type',
                Driver_Id_Data: 'Driver Id Data',
                Trip_Type_Code: 'Trip Type Code',
                Battery_Voltage_V: 'Battery Voltage (V)',
                Analog_2: 'Analog 2',
                Temperature_C: 'Temperature (C)',
                Cellular_Signal_Strength: 'Cellular Signal Strength',
                Temperature_Sensor_C: 'Temperature Sensor (C)',
                Relative_Humidity_Percent: 'Relative Humidity (%)',
                Analog_7: 'Analog 7',
                Analog_8: 'Analog 8',
                Ignition: 'Ignition',
                Tamper: 'Tamper',
                Input_3: 'Input 3',
                Input4: 'Input 4',
                Input_5:  'Input 5',
                Input_6: 'Input 6',
                Input_7: 'Input 7',
                Input_8:  'Input 8',
                Output_1: 'Output 1',
                Output_2: 'Output 2' 
            }; //eo _fields
            const prepare = (_tf, _d, flag) => { 
                /*
                    _tf = tags or fields key -> relabeled key object
                    _d = raw data object
                    flag = if true then convert numeric strings to numbers, else leave alone
                */
                let tf = {};
                let _o = {};
                const mapper = (v,k) => {
                    let val = _d[v]; //set initial value
                    if( _.isNil(val) ) return; //undefined or null do nothing
                    //convert boolean strings to booleans

                    if(_.isString(val) && val.toLowerCase() === 'true') { val = 'TRUE'; }
                    if(_.isString(val) && val.toLowerCase() === 'false') { val = 'FALSE'; }
                    if(flag) { //if flag = true then convert numeric strings to numbers
                        val = (typeof val === 'number' || typeof val === 'string') && !isNaN(Number(val)) ? _.toNumber(val) : val;
                    }
                    _o[k] = val; //set object to santized value
                };
                _.map(_tf, mapper);
                Object.keys(_o).sort().forEach( key => tf[key] = _o[key] );
                return tf;
            }; //eo prepare
            const prepareTime = (_d) => {
                //   { '﻿Date Logged (AET (+10:00))': '27/03/2019 20:52',
                let ts = null;
                let offset = TIMEOFFSET ? delta : 0;
                _.map(Object.keys(_d), (key) => {
                    if(!key.includes('Logged')) { return; }
                    ts = _d[key];
                    ts = moment(ts, 'DD/MM/YYYY HH:mm').valueOf();
                    ts = ts + offset; //move forward in time if turned on
                    counter++;
                    console.log(`counter: ${counter} d: ${_d[key]} offset: ${moment(ts).format('DD/MM/YYYY HH:mm')}`)
                    winston.info(`i: ${counter} ---------------------------------------------`)

                    ts = ts * 1e6;
                })
                return ts;
            }; //eo prepareTime
            let tags = prepare(_tags, d, false);
            let fields = prepare(_fields, d, true);
            let ts = prepareTime(d);
            return {
                measurement: measurement,
                tags: tags,
                fields: fields,
                ts: ts 
            };
        }; //eo body
        return new Promise((resolve) => {
            //const URL = 'https://corlysis.com:8086/write?db=Sensor-Test-Database&u=token&p=32cf43fc0524e292a83b7d3bef9e7764'
            const TOKEN = process.env.CORLYSIS_TOKEN;
            const DB = process.env.CORLYSIS_DB;
            const URL = process.env.CORLYSIS_URL;
            let url = `${URL}/write?db=${DB}&u=token&p=${TOKEN}`;
            let options = {
                method: 'post',
                body:    '', //this gets filled in below
                headers: { 'Content-Type': 'text/plain' },
            };
            const success = (res) => { 
                resolve(`Corlysis response status: ${res.status} message: ${res.statusText}`); 
            };
            const failure = (err) => { 
                reject(err); 
            };
            d.sensorid = measurement
            d['Project Code'] = _.isNil(d['Project Code']) ? measurement : d['Project Code'];
            options.body = jsonToInfluxdbLine.convert(body());
            winston.info(`CORLYSIS_URL: ${url}`);
            winston.info(`CORLYSIS_OPTIONS: ${JSON.stringify(options)}`);
            fetch(url, options).then(success, failure).catch(failure);
        }); //eo promise 
    }; //eo writeData
} //eo class

module.exports = CorlysisInflux;