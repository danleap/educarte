"use strict";

require('dotenv').config();
const driver                          = require('bigchaindb-driver');
const bip39                           = require('bip39');
const base58                          = require('bs58');
const keys                            = require('./GenerateKeyPair.js');
const { from, timer }                 = require('rxjs');
const { map, filter, flatMap, share } = require('rxjs/operators');
const { of }                          = require('rxjs');
const fs                              = require('fs');
const queryString                     = require('qs');
const _                               = require('lodash');
const crypto                          = require('crypto');
const salt                            = require('../utilities/Salt.js');
const aes                             = require('../utilities/AES_GCM_256_Encryption.js');
const winston                         = require('../winston/winston.js')(__filename);
// const Orm                             = require('bigchaindb-orm')
// const ccp                             = require('./CCP_Driver.js');

class Blockchain_Driver {

    constructor(){
        //this.connection = new driver.Connection('https://test.bigchaindb.com/api/v1/');
        this.connection   = new driver.Connection('http://localhost:9984/api/v1/')
        //this.url        = 'https://test.bigchaindb.com/api/v1/';
        //this.connection = new driver.Connection( this.url );
        // this.seed       = bip39.mnemonicToSeed('Four Score and Twenty Years Ago...').slice(0,32);
        // this.pentaKey   = new driver.Ed25519Keypair(this.seed);
        this.pentaKey    = keys.Ed25519Keypair();
        this.salt        = new salt.Salt('sha512', 64);
        this.cipher      = new aes.aes_256_gcm();
        this.blockHeight = 0;
    };

    encryptDataField( field ) {
        if (field){
            if (typeof field == 'string') {
                return this.cipher.encrypt( field );
            } else {
                return this.cipher.encrypt( field.toString() )
            }
        }  else {
            return field;
        }
    };

    async readBlockHeight(){
        let height = 0;
        let at_end = false;
        while ( !at_end ){
            try {
                console.log("Block Reading ... \n")
                await this.connection.getBlock(height)
                        .then( (block) => {
                                            height += 1;
                                            console.log("Block found at " + height + " Transactions : " + JSON.stringify(block))
                                })
                        .catch( (err)  => {at_end = true} );
            }
            catch (err) { at_end = true; }
        };
        console.log("Block Reading Height : " + height + "\n")
        return height;
    }

    prepareTransaction( data ) {
        // TODO: Remove the txBody object and replace it with something more flexible.
        let txBody = {
            txMeasurement            : data.measurement,
            txPressure               : data.fields.Pressure,
            txAmbient                : data.fields.Ambient,
            txTiltShock              : data.fields.Tilt_Shock,
            txTemperatureReadingC    : data.fields.Temperature_C,
            txTemperatureReadingF    : data.fields.Temperature_F,
            txTemperatureBoard       : data.fields.Temperature_sensor_C,
            txRelativeHumidity       : data.fields.Relative_Humidity_Percent,
            txTagSerialNumber        : data.SensorID,
            txSensorType             : "DigitalMatter.Sting",
            txLatitude               : data.fields.Latitude,
            txLongtitude             : data.fields.Longitude,
            txTamper                 : data.fields.Tamper,
            txSupplyChainState       : data.fields.Supply_Chain_State,
            txTimeStamp              : data.timestamp,
            txBatteryVoltage         : data.fields.Battery_Voltage_V,
            txBatteryVoltagePercent  : data.fields.Battery_Voltage_Percent,
            txCellularSignalStrength : data.fields.Cellular_Signal_Strength,
            txDeviceName             : data.fields.Device_Name,
            txDriverID               : data.fields.Driver_Id_Data,
            txDriverIDType           : data.fields.Driver_Id_Type,
            txGSPStatusFix3D         : data.fields.Gps_Status_Fix_3D,
            txGPSStatusFixOK         : data.fields.Gps_Status_Fix_OK,
            txHeadingDegress         : data.fields.Heading_Degrees,
            txIgnition               : data.fields.Ignition,
            txSpeedAccuracyKmH       : data.fields.Speed_Accuracy_KmH,
            txSpeedKmH               : data.fields.Speed_KmH,
            txLuminosity             : data.fields.Luminosity,
            txTripTypeCode           : data.fields.Trip_Type_Code,
            txLogged                 : data.fields.Logged,
            txStatus                 : data.fields.Status
        };
        let txMeta = {
            Company       : this.cipher.encrypt( data.tags.Company ),
            Project       : this.cipher.encrypt( data.tags.Project ),
            Tag_Data_Hash : "",
            meta_hash     : ""
        };
        //  Encode the body before placing the contents on the blockchain.
        //  To work with the data on the chain, the masterkey for the
        //  aes 256 encryption is needed, and the
        txMeta.meta_hash  = this.salt.saltData( JSON.stringify(txBody) );
        Object.keys( txBody ).map( (key, index) => {
            if ( txBody[key] ){
                if (typeof txBody[key] == 'string') {
                    txBody[key] = this.cipher.encrypt( txBody[key] );
                } else {
                    txBody[key] = this.cipher.encrypt( txBody[key].toString() );
                }
            }
        });
        return { body : txBody, meta : txBody };
    }

    async writeTransactions( data ) {
        //  We are dealing with arrays of points. Currently we upload each point indvidually but
        //  encrypt it first. Latter we will add 'blocking'
        //
        //  1. Ensure that we are dealing with an array of points.
        let sensorData = _.isArray(data) ? data : [data];
        await this.readBlockHeight()
                .then( (height) => console.log("Block height at start : " + height))
                .catch( (err)   => console.log("Write Transaction Error : " + err));
        //  2. For each point, prepare it by extracting the data we need, encrypting it and then
        //  removing any null or undefined fields.
        let txData = [];
        await Promise.all( sensorData.map( async (sensorReading) => {
                            let transaction = this.prepareTransaction( sensorReading );
                            let txBody      = transaction.body;
                            let txMeta      = transaction.meta;
                            let tx          = driver.Transaction.makeCreateTransaction(
                                                    txBody,
                                                    txMeta,
                                                    [ driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(this.pentaKey.publicKey)) ],
                                                    this.pentaKey.publicKey
                                                );
                            let txSigned = driver.Transaction.signTransaction(tx, this.pentaKey.privateKey);
                            let txOut = {};
                            await this.connection.postTransactionCommit( txSigned )
                                    .then( (txRetrieved) => {
                                                txOut = txRetrieved;
                                            })
                                    .catch( (err) => winston.info("Bigchain Error : " + err) );
                            //  3. Now that we have a Bigchain transaction id we can add it to our encrypted data in
                            //  preparation for pushing it to influx.
                            sensorReading.BigChain_Hash = txOut.id;
                            winston.info('Asset Created: ' + sensorReading.BigChain_Hash + '\n');
                            await this.connection.getTransaction(txOut.id)
                                    .then( (txSaved) => {
                                                console.log("Retreived : " + JSON.stringify(txSaved) + "\n\n");
                                            })
                                    .catch( (err) => winston.info("Bigchain Retrieval Error : " + JSON.stringify(err)));
                            txData.push(sensorReading);
        }));
        await this.readBlockHeight()
                .then( (height) => console.log("Block height at start : " + height))
                .catch( (err)   => console.log("Write Transaction Error : " + err));
        //  4. Return the original data with the Bigchain tx identity included.
        // console.log('Blockchain: txData ' + JSON.stringify( txData ) + '\n\n');
        return txData;
    };

    async readBlocks(){
        console.log("Not yet  ... \n")
    }

    async readByTransactionId( txHash ) {
        console.log("Not yet  ... \n")
    };

    async readByAssetPattern( txAssetPattern ){
        console.log("Not yet  ... \n")
    }

    async readMetaPattern( txMetaPattern ) {
        console.log("Not yet ... \n")
    }
};

module.exports.Blockchain_Driver=Blockchain_Driver;
